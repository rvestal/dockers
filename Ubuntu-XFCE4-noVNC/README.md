# Ubuntu XFCE4-noVNC Image

This will create a base Ubuntu container with XFCE4 and uses noVNC for connecting. This is *not* a hardened docker at this point. This is licensed under GPLv2.

To build, simply download the repo, cd to the appropriate folder and then:


> docker build -t your_image_name .  (Don't forget that period!)


Then simply run a container, exposing port 6080 for noVNC to work.


> docker run -it -d -p 6080:6080 --name your_container_name your_image_name

Next open a browser and browse to http://localhost:6080

See the Docker documentation for more information
[Docker Reference Library](https://docs.docker.com/reference)
