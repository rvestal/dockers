## vmware-pyvmomi image

This docker image is based on the latest ubuntu image and has python3 installed and the VMware pyvmomi library installed using pip, and copies the pyvmomi-community-samples to /pyvmomi-community-samples. To build, simply download the repo, cd to the appropriate folder and then:

> docker build -t your_image_name .  (Don't forget that period!)

Then simply run a container. You will be dropped into the container once running:

> docker run -it  --name your_container_name your_image_name /bin/bash

See the Docker documentation for more information
[Docker Reference Library](https://docs.docker.com/reference)
