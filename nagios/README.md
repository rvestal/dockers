# CentOS 7 based Nagios server

This will create a CentOS 7 based Nagios server. This is *not* a hardened docker at this point. This is licensed under GPLv2. This installs Apache2 httpd server, PHP5 and Nagios 4.2.4, and starts the server automatically.

By default, port 80 is exposed internally.

There are 2 test files to verify the docker built correctly. These should be removed after the deployment is verified:
* /var/www/html/index.html
* /var/www/html/info.php

Nagios is built based on the current install documentation for Nagios 4.2.0. The documentation is found here:

[Nagios Installation Documentation](https://assets.nagios.com/downloads/nagioscore/docs/Installing_Nagios_Core_From_Source.pdf)

---

To build this docker, simply download the repo, cd to the appropriate folder and then:


> docker build -t your_image_name .  (Don't forget that period!)


Then simply run a container. This example exposes the internal port 80 to the external port 80.

> docker run -it -d -p 80:80 --name your_container_name your_image_name

To verify the build, browse to http://localhost:80 for the default test files

For Nagios, browse to http://localhost:80/nagios

The default username/password is nagiosadmin/nagiosadmin. **Please change the password immedately** 

See the Docker documentation for more information
[Docker Reference Library](https://docs.docker.com/reference)
