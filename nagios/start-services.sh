#!/bin/bash

# Start Apache daemon
rm -rf /run/httpd/* /tmp/httpd*

exec /usr/sbin/apachectl -DFOREGROUND &
exec /usr/local/nagios/bin/nagios /usr/local/nagios/etc/nagios.cfg &
while true; do sleep 2; done
