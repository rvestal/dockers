## Docker Cheat Sheet

This is my Docker cheat sheet. It's not complete, as I'm still learning Docker.

HTH,
-Roy

---

To build a new docker:

* Create a directory for the docker (case sensitive)
* Create a Dockerfile 
 * FROM os_name *Required* <- The OS that is to be used for the image
 * MAINTAINER <- Maintainer information
 * LABEL <- A label to add to the docker
 * EXPOSE <- To expose a port outside of the container
 * ADD <- Copies a file from the docker folder into the image during build
 * RUN <- Runs a command during the image build
 * CMD [" command_here"] <- Runs a command when starting a container. Only the last CMD will be run.

---

Once the Dockerfile , and any support files, are created, run the build command to create the image:

> docker build -t your_image_name .  (Don't forget that period!)

---

To run a container. This example exposes the internal port 80 to the external port 80.

> docker run -it -d -p 80:80 --name your_container_name your_image_name

--- 

To attach to a running container bash terminal:

> docker exec -it your_container /bin/bash

---

To delete all containers, run:

> docker rm -f $(docker ps -a -q)

---

To remove all images, stop and remove all containers, then run:

> docker rmi -f $(docker images -q)
