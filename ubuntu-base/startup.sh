#!/usr/bin/env bash
set -e

## Run startup command 
echo "Hello ENTRYPOINT" 

## Running passed command
if [[ "$1" ]]; then
	eval "$@"
fi
