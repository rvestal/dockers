## dockers

These are the docker files I've created for my work. These are shared GPLv2 unless otherwise noted.

To build, simply download the repo, cd to the appropriate folder and then:

> docker build -t your_image_name .  (Don't forget that period!)

Then simply run a container. 

> docker run -it  --name your_container_name your_image_name

See the Docker documentation for more information
[Docker Reference Library](https://docs.docker.com/reference)