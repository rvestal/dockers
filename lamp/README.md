# CentOS 7 based LAMP server

This will create a CentOS 7 based LAMP server. This is *not* a hardened docker at this point. This is licensed under GPLv2. This installs Apache2 httpd server and PHP, and starts the server automatically.

**At this time, *SQL is not installed**

* By default, port 80 is exposed internally.

There are 2 test files to verify the docker built correctly:
  /var/www/html/index.html
  /var/www/html/info.php

To build, simply download the repo, cd to the appropriate folder and then:


> docker build -t your_image_name .  (Don't forget that period!)


Then simply run a container. This example exposes the internal port 80 to the external port 80.


> docker run -it -d -p 80:80 --name your_container_name your_image_name

See the Docker documentation for more information
[Docker Reference Library](https://docs.docker.com/reference)